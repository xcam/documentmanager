-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.13-log - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2014-03-23 21:35:22
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping structure for table documents.application_for_payment
CREATE TABLE IF NOT EXISTS `application_for_payment` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `CONTRACTOR` varchar(256) DEFAULT NULL,
  `CURRENCY` varchar(256) DEFAULT NULL,
  `EXCHANGE_RATE` double DEFAULT NULL,
  `FEE` double DEFAULT NULL,
  PRIMARY KEY (`ID`),
  CONSTRAINT `FK__DOCUMENT_APPLICATION_FOR_PAYMENT` FOREIGN KEY (`ID`) REFERENCES `document` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table documents.bill_of_parcels
CREATE TABLE IF NOT EXISTS `bill_of_parcels` (
  `ID` bigint(20) unsigned NOT NULL,
  `CURRENCY` varchar(256) DEFAULT NULL,
  `EXCHANGE_RATE` double unsigned NOT NULL,
  `ARTICLE` varchar(256) NOT NULL,
  `QUANTITY` double unsigned NOT NULL,
  PRIMARY KEY (`ID`),
  CONSTRAINT `FK__document_BILL_OF_PARCELS` FOREIGN KEY (`ID`) REFERENCES `document` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table documents.document
CREATE TABLE IF NOT EXISTS `document` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `NUMBER` varchar(256) DEFAULT NULL,
  `USER` varchar(256) DEFAULT NULL,
  `SUM` double DEFAULT NULL,
  `DATE` timestamp NULL DEFAULT NULL,
  `TYPE` varchar(256) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table documents.payment_document
CREATE TABLE IF NOT EXISTS `payment_document` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `EMPLOYEE` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  CONSTRAINT `FK__document_PAYMENT_DOCUMENT` FOREIGN KEY (`ID`) REFERENCES `document` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

package ru.atol.document.manager.view.component;

import ru.atol.document.manager.model.entity.Entity;

public interface TableController<T extends Entity> {
	public void refresh();
}

package ru.atol.document.manager.view.component.pagingtable;

import java.util.LinkedList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import ru.atol.document.manager.model.entity.Entity;

public abstract class PagingTableModel<T extends Entity> extends
		AbstractTableModel {
	private static final long serialVersionUID = -3250329967471387734L;

	protected List<T> data;

	public PagingTableModel() {
		data = new LinkedList<T>();
	}

	@Override
	public int getRowCount() {
		return data.size();
	}

	public void setData(List<T> data) {
		this.data.clear();
		this.data.addAll(data);
		fireTableDataChanged();
	}
}

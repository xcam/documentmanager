package ru.atol.document.manager.view.component.pagingtable;

import java.util.LinkedList;
import java.util.List;

import javax.swing.JPanel;

import ru.atol.document.manager.model.dao.filter.Filter;
import ru.atol.document.manager.view.listener.DataChangeListener;

public abstract class FilterPanel extends JPanel {

	private static final long serialVersionUID = 7082047516363136750L;
	private List<DataChangeListener> listeners;

	public FilterPanel() {
		listeners = new LinkedList<>();
		init();
		initListeners();
	}

	public abstract Filter getFilter();

	public abstract void init();

	public abstract void initListeners();

	public void addDataChangeListener(DataChangeListener dataChangeListener) {
		if (dataChangeListener != null) {
			listeners.add(dataChangeListener);
		}
	}

	protected void fireDataChangeListenersEvent() {
		for (DataChangeListener dataChangeListener : listeners) {
			dataChangeListener.dataChanged();
		}
	}
}

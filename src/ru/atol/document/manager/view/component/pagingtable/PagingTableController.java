package ru.atol.document.manager.view.component.pagingtable;

import java.util.List;

import ru.atol.document.manager.model.dao.PageableDAO;
import ru.atol.document.manager.model.entity.Entity;
import ru.atol.document.manager.view.component.TableController;
import ru.atol.document.manager.view.listener.DataChangeListener;

public class PagingTableController<T extends Entity> implements
		TableController<T>, DataChangeListener {
	private PagingTable<T> pagingTable;
	private PageableDAO<T> pageableDAO;

	private int pageNumber = 1;
	private int pageSize = 10;

	public PagingTableController(PagingTable<T> pagingTable,
			PageableDAO<T> pageableDAO) {
		this.pagingTable = pagingTable;
		this.pageableDAO = pageableDAO;
	}

	public void previousPage() {
		this.pageNumber--;
		refresh();
	}

	public void nextPage() {
		this.pageNumber++;
		refresh();
	}

	public void goToPage(int pageNumber) {
		this.pageNumber = pageNumber;
		refresh();
	}

	public void refresh() {
		if (getOffset() >= pageableDAO.quantity(pagingTable.getFilter())) {
			pageNumber = (pageableDAO.quantity(pagingTable.getFilter()) - 1)
					/ pageSize + 1;
		}

		if (pageNumber <= 0) {
			pageNumber = 1;
		}

		List<T> data = pageableDAO.getData(getOffset(), pageSize,
				pagingTable.getFilter());
		pagingTable.setCurrentPage(pageNumber);
		pagingTable.getPagingTableModel().setData(data);
	}

	private int getOffset() {
		return (pageNumber - 1) * pageSize;
	}

	@Override
	public void dataChanged() {
		refresh();
	}
}

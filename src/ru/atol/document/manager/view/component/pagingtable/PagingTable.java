package ru.atol.document.manager.view.component.pagingtable;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import ru.atol.document.manager.model.dao.PageableDAO;
import ru.atol.document.manager.model.dao.filter.Filter;
import ru.atol.document.manager.model.entity.Entity;
import ru.atol.document.manager.utils.NumberUtils;
import ru.atol.document.manager.view.listener.IntegerDocumentListener;

public class PagingTable<T extends Entity> extends JPanel {
	private static final long serialVersionUID = 16683721755826442L;

	private JButton previousPageBtn;
	private JButton nextPageBtn;
	private JTextField currentPageTxtField;
	private JScrollPane scrollPane;
	private JTable table;
	private PagingTableController<T> pagingTableController;
	private PagingTableModel<T> pagingTableModel;
	private FilterPanel filterPanel;

	public PagingTable(PagingTableModel<T> pagingTableModel, JTable table,
			PageableDAO<T> pageableDAO, FilterPanel filterPanel) {
		this.pagingTableModel = pagingTableModel;
		this.table = table;
		pagingTableController = new PagingTableController<T>(this, pageableDAO);
		this.filterPanel = filterPanel;
		init();
		initListeners();
	}

	private void initListeners() {
		previousPageBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				pagingTableController.previousPage();
			}
		});

		nextPageBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				pagingTableController.nextPage();
			}
		});

		if (filterPanel != null) {
			filterPanel.addDataChangeListener(pagingTableController);
		}
	}

	private void init() {
		setLayout(new BorderLayout());
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout());

		previousPageBtn = new JButton("<-");
		nextPageBtn = new JButton("->");
		currentPageTxtField = new JTextField();
		currentPageTxtField.setMinimumSize(new Dimension(50,
				currentPageTxtField.getMinimumSize().height));
		currentPageTxtField.setPreferredSize(new Dimension(50,
				currentPageTxtField.getMinimumSize().height));
		currentPageTxtField.getDocument().addDocumentListener(
				new IntegerDocumentListener(currentPageTxtField));
		currentPageTxtField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if (e.getKeyChar() == KeyEvent.VK_ENTER) {
					pagingTableController.goToPage(NumberUtils
							.parseInt(currentPageTxtField.getText()));
				}
			}
		});

		panel.add(previousPageBtn);
		panel.add(currentPageTxtField);
		panel.add(nextPageBtn);

		add(panel, BorderLayout.SOUTH);
		if (filterPanel != null) {
			add(filterPanel, BorderLayout.NORTH);
		}

		scrollPane = new JScrollPane(table);
		add(scrollPane);
	}

	public void setCurrentPage(int pageNumber) {
		currentPageTxtField.setText(Integer.toString(pageNumber));
	}

	public PagingTableController<T> getPagingTableController() {
		return pagingTableController;
	}

	public PagingTableModel<T> getPagingTableModel() {
		return pagingTableModel;
	}

	public Filter getFilter() {
		if (filterPanel != null) {
			return filterPanel.getFilter();
		}

		return null;
	}
}

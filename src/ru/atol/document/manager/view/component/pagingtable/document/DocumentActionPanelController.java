package ru.atol.document.manager.view.component.pagingtable.document;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import ru.atol.document.manager.model.dao.DAOFactory;
import ru.atol.document.manager.model.entity.document.Document;
import ru.atol.document.manager.view.MainFrame;
import ru.atol.document.manager.view.component.ActionPanelController;
import ru.atol.document.manager.view.component.document.dialog.DocumentDialog;

public class DocumentActionPanelController implements
		ActionPanelController<Document> {

	private static Logger log = Logger
			.getLogger(DocumentActionPanelController.class.getName());

	public DocumentActionPanelController() {
	}

	@Override
	public void edit(Document value) {
		DocumentDialog<Document> documentDialog = DocumentDialog.createDialog(
				value, "Редактировать");
		if (documentDialog == null) {
			log.log(Level.SEVERE, "Документ " + value + " не найден");
			JOptionPane.showMessageDialog(null, "Документ не найден",
					"Ошибка редактирования", JOptionPane.ERROR_MESSAGE, null);
		} else {
			documentDialog.show();
		}
		MainFrame.refresh();
	}

	@Override
	public void delete(Document value) {
		DAOFactory.getDefaultDAOFactory().getDocumentDAO().delete(value);
		MainFrame.refresh();
	}
}

package ru.atol.document.manager.view.component.pagingtable.document;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTextField;

import org.jdesktop.swingx.JXDatePicker;

import ru.atol.document.manager.model.dao.filter.DateCriteria;
import ru.atol.document.manager.model.dao.filter.DocumentFilter;
import ru.atol.document.manager.model.dao.filter.Filter;
import ru.atol.document.manager.model.dao.filter.StringCriteria;
import ru.atol.document.manager.view.component.pagingtable.FilterPanel;

public class DocumentFilterPanel extends FilterPanel {

	private static final long serialVersionUID = -6544322068608163797L;
	private DocumentFilter filter;

	private JCheckBox numberCheckBox;
	private JLabel numberLabel;
	private JTextField numberTxtField;

	private JCheckBox dateCheckBox;
	private JLabel dateAfterLabel;
	private JXDatePicker dateAfterDatePicker;
	private JLabel dateBeforeLabel;
	private JXDatePicker dateBeforeDatePicker;

	private JButton applyBtn;

	public DocumentFilterPanel() {
		filter = new DocumentFilter();
	}

	@Override
	public void init() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		setBorder(BorderFactory.createEmptyBorder(0, 3, 0, 3));

		Box numberBox = Box.createHorizontalBox();
		numberBox.setAlignmentX(LEFT_ALIGNMENT);
		numberBox.setBorder(BorderFactory.createEmptyBorder(4, 0, 2, 0));
		numberCheckBox = new JCheckBox();
		numberLabel = new JLabel("Номер: ");
		numberTxtField = new JTextField();

		numberBox.add(numberCheckBox);
		numberBox.add(numberLabel);
		numberBox.add(numberTxtField);

		Box dateBox = Box.createHorizontalBox();
		dateBox.setAlignmentX(LEFT_ALIGNMENT);
		dateBox.setBorder(BorderFactory.createEmptyBorder(2, 0, 2, 0));

		dateCheckBox = new JCheckBox();
		dateAfterLabel = new JLabel("Интервал дат: позже ");
		dateAfterDatePicker = new JXDatePicker();
		dateBeforeLabel = new JLabel(", но раньше ");
		dateBeforeDatePicker = new JXDatePicker();
		dateAfterDatePicker.setDate(new Date());
		dateBeforeDatePicker.setDate(new Date());

		dateBox.add(dateCheckBox);
		dateBox.add(dateAfterLabel);
		dateBox.add(dateAfterDatePicker);
		dateBox.add(dateBeforeLabel);
		dateBox.add(dateBeforeDatePicker);

		Box actionBox = Box.createHorizontalBox();
		actionBox.setAlignmentX(LEFT_ALIGNMENT);
		actionBox.setBorder(BorderFactory.createEmptyBorder(2, 0, 4, 0));
		applyBtn = new JButton("Применить");
		actionBox.add(applyBtn);

		add(numberBox);
		add(dateBox);
		add(actionBox);

		setNumberFilterEnable(false);
		setDateFilterEnable(false);
	}

	@Override
	public void initListeners() {
		applyBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				updateFilter();
				fireDataChangeListenersEvent();
			}
		});

		numberCheckBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setNumberFilterEnable(numberCheckBox.isSelected());
			}
		});

		dateCheckBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setDateFilterEnable(dateCheckBox.isSelected());
			}
		});
	}

	@Override
	public Filter getFilter() {
		return filter;
	}

	private void updateFilter() {
		if (numberCheckBox.isSelected()) {
			StringCriteria stringCriteria = new StringCriteria(
					numberTxtField.getText());
			filter.setNumberCriteria(stringCriteria);
		} else {
			filter.setNumberCriteria(null);
		}

		if (dateCheckBox.isSelected()) {
			DateCriteria dateCriteria = new DateCriteria(
					dateAfterDatePicker.getDate(),
					dateBeforeDatePicker.getDate());
			filter.setDateCriteria(dateCriteria);
		} else {
			filter.setDateCriteria(null);
		}
	}

	private void setDateFilterEnable(boolean enable) {
		dateAfterLabel.setEnabled(enable);
		dateAfterDatePicker.setEnabled(enable);
		dateBeforeLabel.setEnabled(enable);
		dateBeforeDatePicker.setEnabled(enable);
	}

	private void setNumberFilterEnable(boolean enable) {
		numberLabel.setEnabled(enable);
		numberTxtField.setEnabled(enable);
	}
}

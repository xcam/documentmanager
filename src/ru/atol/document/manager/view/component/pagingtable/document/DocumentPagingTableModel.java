package ru.atol.document.manager.view.component.pagingtable.document;

import java.util.HashMap;
import java.util.Map;

import ru.atol.document.manager.model.entity.document.Document;
import ru.atol.document.manager.view.component.ActionPanel;
import ru.atol.document.manager.view.component.ActionPanelController;
import ru.atol.document.manager.view.component.pagingtable.PagingTableModel;

public class DocumentPagingTableModel extends PagingTableModel<Document> {

	private static final long serialVersionUID = -7675328877887560389L;

	private final String[] COLUMN_NAME = { "ID", "Номер", "Дата",
			"Пользователь", "Сумма", "Действия" };
	private final static int COLUMN_INDEX_ID = 0;
	private final static int COLUMN_INDEX_NUMBER = COLUMN_INDEX_ID + 1;
	private final static int COLUMN_INDEX_DATE = COLUMN_INDEX_NUMBER + 1;
	private final static int COLUMN_INDEX_USER = COLUMN_INDEX_DATE + 1;
	private final static int COLUMN_INDEX_SUM = COLUMN_INDEX_USER + 1;
	private final static int COLUMN_INDEX_ACTION = COLUMN_INDEX_SUM + 1;

	private ActionPanelController<Document> actionPanelController;
	private Map<Integer, ActionPanel<Document>> actionPanels = new HashMap<Integer, ActionPanel<Document>>();

	@Override
	public String getColumnName(int column) {
		return COLUMN_NAME[column];
	}

	@Override
	public int getColumnCount() {
		return COLUMN_NAME.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Document document = data.get(rowIndex);
		ActionPanel<Document> actionPanel = getActionPanel(rowIndex, document);
		switch (columnIndex) {
		case COLUMN_INDEX_ID:
			return document.getId();
		case COLUMN_INDEX_NUMBER:
			return document.getNumber();
		case COLUMN_INDEX_DATE:
			return document.getDate();
		case COLUMN_INDEX_USER:
			return document.getUser();
		case COLUMN_INDEX_SUM:
			return document.getSum();
		case COLUMN_INDEX_ACTION:
			return actionPanel;
		}
		return null;
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return columnIndex == COLUMN_INDEX_ACTION;
	}

	public void setActionPanelController(
			ActionPanelController<Document> actionPanelController) {
		this.actionPanelController = actionPanelController;
	}

	private ActionPanel<Document> getActionPanel(int row, Document document) {
		ActionPanel<Document> actionPanel = actionPanels.get(row);
		if (actionPanel == null) {
			actionPanel = new ActionPanel<Document>(document,
					actionPanelController);
			actionPanels.put(row, actionPanel);
		}

		actionPanel.setValue(document);
		return actionPanel;
	}
}

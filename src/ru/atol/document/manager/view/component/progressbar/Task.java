package ru.atol.document.manager.view.component.progressbar;

import java.awt.Cursor;
import java.awt.Toolkit;

import javax.swing.SwingWorker;

import ru.atol.document.manager.view.MainFrame;

public abstract class Task extends SwingWorker<Void, Void> {
	private ProgressBarDialog progressBar;

	public static final int MAX_PROGRESS_VALUE = 100;

	@Override
	public Void doInBackground() {
		progressBar = new ProgressBarDialog(MAX_PROGRESS_VALUE);
		addPropertyChangeListener(progressBar);
		progressBar.setVisible(true);
		progressBar.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		setProgress(0);
		MainFrame.getCurrent().setEnabled(false);
		process();
		return null;
	}

	protected abstract void process();

	/*
	 * Executed in event dispatching thread
	 */
	@Override
	public void done() {
		progressBar.setCursor(null);
		MainFrame.refresh();
		MainFrame.getCurrent().setEnabled(true);
		progressBar.dispose();
		Toolkit.getDefaultToolkit().beep();
	}
}
package ru.atol.document.manager.view.component.progressbar;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JDialog;
import javax.swing.JProgressBar;

import ru.atol.document.manager.utils.GUIUtils;

public class ProgressBarDialog extends JDialog implements
		PropertyChangeListener {

	private static final long serialVersionUID = -7914447408412654850L;
	private JProgressBar progressBar;

	public ProgressBarDialog(int maxValue) {
		progressBar = new JProgressBar(0, maxValue);
		progressBar.setValue(0);
		progressBar.setStringPainted(true);

		getContentPane().add(progressBar);
		setUndecorated(true);
		GUIUtils.moveToCenter(this);
		pack();
	}

	public void propertyChange(PropertyChangeEvent evt) {
		if ("progress" == evt.getPropertyName()) {
			int progress = (Integer) evt.getNewValue();
			progressBar.setValue(progress);
		}
	}
}

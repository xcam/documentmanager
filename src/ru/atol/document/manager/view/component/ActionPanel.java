package ru.atol.document.manager.view.component;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import ru.atol.document.manager.model.entity.Entity;

public class ActionPanel<T extends Entity> extends JPanel {
	private static final long serialVersionUID = 89546665489101312L;
	private static ImageIcon editIcon;
	private static ImageIcon deleteIcon;
	private JButton editBtn;
	private JButton deleteBtn;
	private ActionPanelController<T> actionPanelController;

	private T value;

	static {
		try {
			editIcon = new ImageIcon(ImageIO.read(new File(
					"resources/img/edit.png")));
			deleteIcon = new ImageIcon(ImageIO.read(new File(
					"resources/img/delete.png")));
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}

	public ActionPanel(T value, ActionPanelController<T> actionPanelController) {
		this.value = value;
		this.actionPanelController = actionPanelController;
		init();
		initListener();
	}

	private void init() {
		setLayout(new GridLayout(0, 2));
		editBtn = new JButton("");
		editBtn.setIcon(editIcon);
		deleteBtn = new JButton("");
		deleteBtn.setIcon(deleteIcon);

		add(editBtn);
		add(deleteBtn);
	}

	private void initListener() {
		editBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				actionPanelController.edit(value);
			}
		});

		deleteBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				actionPanelController.delete(value);
			}
		});
	}

	public void setValue(T value) {
		this.value = value;
	}
}

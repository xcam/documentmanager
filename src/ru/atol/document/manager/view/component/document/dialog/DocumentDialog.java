package ru.atol.document.manager.view.component.document.dialog;

import javax.swing.JOptionPane;

import ru.atol.document.manager.model.dao.Crud;
import ru.atol.document.manager.model.dao.DAOFactory;
import ru.atol.document.manager.model.entity.Entity;
import ru.atol.document.manager.model.entity.document.ApplicationForPayment;
import ru.atol.document.manager.model.entity.document.BillOfParcels;
import ru.atol.document.manager.model.entity.document.Document;
import ru.atol.document.manager.model.entity.document.PaymentDocument;
import ru.atol.document.manager.view.MainFrame;
import ru.atol.document.manager.view.component.document.ApplicationForPaymentPanel;
import ru.atol.document.manager.view.component.document.BillOfParcelsPanel;
import ru.atol.document.manager.view.component.document.DocumentPanel;
import ru.atol.document.manager.view.component.document.PaymentDocumentPanel;

public class DocumentDialog<T extends Document> {
	private T document;
	private DocumentPanel<T> panel;
	private String title;
	private Crud<T> crudDAO;

	public DocumentDialog(T document, DocumentPanel<T> panel, String title,
			Crud<T> crudDAO) {
		this.document = document;
		this.panel = panel;
		this.title = title;
		this.crudDAO = crudDAO;
	}

	public void show() {
		int result = JOptionPane.showOptionDialog(null, panel, title,
				JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null,
				null, null);

		if (result == JOptionPane.OK_OPTION) {
			panel.doFinish();
			crudDAO.saveOrUpdate(document);
			MainFrame.refresh();
		}
	}

	@SuppressWarnings("unchecked")
	public static <A extends Document> DocumentDialog<A> createDialog(
			A document, String title) {
		if (document instanceof BillOfParcels) {
			Crud<BillOfParcels> dao = DAOFactory.getDefaultDAOFactory()
					.getBillOfParcelsDAO();
			BillOfParcels billOfParcels;
			if (document.getId() != Entity.ID_NOT_SET) {
				billOfParcels = dao.getByID(document.getId());
			} else {
				billOfParcels = (BillOfParcels) document;
			}

			if (billOfParcels == null) {
				return null;
			}

			return (DocumentDialog<A>) new DocumentDialog<BillOfParcels>(
					billOfParcels, new BillOfParcelsPanel(billOfParcels),
					title, dao);
		}

		if (document instanceof ApplicationForPayment) {
			Crud<ApplicationForPayment> dao = DAOFactory.getDefaultDAOFactory()
					.getApplicationForPaymentDAO();

			ApplicationForPayment applicationForPayment;
			if (document.getId() != Entity.ID_NOT_SET) {
				applicationForPayment = dao.getByID(document.getId());
			} else {
				applicationForPayment = (ApplicationForPayment) document;
			}

			if (applicationForPayment == null) {
				return null;
			}

			return (DocumentDialog<A>) new DocumentDialog<ApplicationForPayment>(
					applicationForPayment, new ApplicationForPaymentPanel(
							applicationForPayment), title, dao);
		}

		if (document instanceof PaymentDocument) {
			Crud<PaymentDocument> dao = DAOFactory.getDefaultDAOFactory()
					.getPaymentDocumentDao();

			PaymentDocument paymentDocument;
			if (document.getId() != Entity.ID_NOT_SET) {
				paymentDocument = dao.getByID(document.getId());
			} else {
				paymentDocument = (PaymentDocument) document;
			}

			if (paymentDocument == null) {
				return null;
			}

			return (DocumentDialog<A>) new DocumentDialog<PaymentDocument>(
					paymentDocument, new PaymentDocumentPanel(paymentDocument),
					title, dao);
		}

		return null;
	}
}

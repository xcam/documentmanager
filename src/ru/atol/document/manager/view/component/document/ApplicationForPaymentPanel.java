package ru.atol.document.manager.view.component.document;

import javax.swing.JLabel;
import javax.swing.JTextField;

import ru.atol.document.manager.model.entity.document.ApplicationForPayment;
import ru.atol.document.manager.utils.NumberUtils;
import ru.atol.document.manager.view.listener.DoubleDocumentListener;

public class ApplicationForPaymentPanel extends
		DocumentPanel<ApplicationForPayment> {

	private static final long serialVersionUID = -7749478239059529271L;
	private JTextField txtFieldContractor;
	private JTextField txtFieldCurrency;
	private JTextField txtFieldExchangeRate;
	private JTextField txtFieldFee;

	public ApplicationForPaymentPanel(ApplicationForPayment document) {
		super();
		init(document);
	}

	@Override
	protected void createFields() {
		super.createFields();

		txtFieldCurrency = new JTextField();
		txtFieldExchangeRate = new JTextField();
		txtFieldExchangeRate.getDocument().addDocumentListener(
				new DoubleDocumentListener(txtFieldExchangeRate));
		txtFieldFee = new JTextField();
		txtFieldFee.getDocument().addDocumentListener(
				new DoubleDocumentListener(txtFieldFee));
		txtFieldContractor = new JTextField();

		add(new JLabel("Валюта:"));
		add(txtFieldCurrency);
		add(new JLabel("Курс валюты:"));
		add(txtFieldExchangeRate);
		add(new JLabel("Комиссия:"));
		add(txtFieldFee);
		add(new JLabel("Контрагент:"));
		add(txtFieldContractor);
	}

	@Override
	public void init(ApplicationForPayment document) {
		super.init(document);
		txtFieldCurrency.setText(document.getCurrency() == null ? "" : document
				.getCurrency());
		txtFieldExchangeRate
				.setText(Double.toString(document.getExchangeRate()));
		txtFieldFee.setText(Double.toString(document.getFee()));
		txtFieldContractor.setText(document.getContractor() == null ? ""
				: document.getContractor());
	}

	@Override
	public void doFinish() {
		super.doFinish();
		document.setCurrency(txtFieldCurrency.getText().trim());
		document.setExchangeRate(NumberUtils.parseDouble(txtFieldExchangeRate
				.getText().trim()));
		document.setFee(NumberUtils.parseDouble(txtFieldFee.getText().trim()));
		document.setContractor(txtFieldContractor.getText().trim());

	}
}

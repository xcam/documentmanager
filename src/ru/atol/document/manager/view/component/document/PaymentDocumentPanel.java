package ru.atol.document.manager.view.component.document;

import javax.swing.JLabel;
import javax.swing.JTextField;

import ru.atol.document.manager.model.entity.document.PaymentDocument;

public class PaymentDocumentPanel extends DocumentPanel<PaymentDocument> {

	private static final long serialVersionUID = 2018583891330644660L;
	private JTextField txtFieldEmployee;

	public PaymentDocumentPanel(PaymentDocument document) {
		super();
		init(document);
	}

	@Override
	protected void createFields() {
		super.createFields();

		txtFieldEmployee = new JTextField();

		add(new JLabel("Сотрудник:"));
		add(txtFieldEmployee);
	}

	@Override
	public void init(PaymentDocument document) {
		super.init(document);

		txtFieldEmployee.setText(document.getEmployee() == null ? "" : document
				.getEmployee());
	}

	@Override
	public void doFinish() {
		super.doFinish();
		document.setEmployee(txtFieldEmployee.getText().trim());
	}
}

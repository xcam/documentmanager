package ru.atol.document.manager.view.component.document;

import java.awt.GridLayout;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.jdesktop.swingx.JXDatePicker;

import ru.atol.document.manager.model.entity.document.Document;
import ru.atol.document.manager.utils.NumberUtils;
import ru.atol.document.manager.view.listener.DoubleDocumentListener;

public abstract class DocumentPanel<T extends Document> extends JPanel {

	private static final long serialVersionUID = -8684934833861326834L;
	protected T document;
	private JTextField txtFieldNumber;
	private JXDatePicker datePicker;
	private JTextField txtFieldUser;
	private JTextField txtFieldSum;

	public DocumentPanel() {
		createFields();
	}

	protected void createFields() {
		setLayout(new GridLayout(0, 2));

		txtFieldNumber = new JTextField();
		datePicker = new JXDatePicker();
		txtFieldUser = new JTextField();
		txtFieldSum = new JTextField();
		txtFieldSum.getDocument().addDocumentListener(
				new DoubleDocumentListener(txtFieldSum));

		add(new JLabel("Номер:"));
		add(txtFieldNumber);
		add(new JLabel("Дата:"));
		add(datePicker);
		add(new JLabel("Пользователь:"));
		add(txtFieldUser);
		add(new JLabel("Сумма:"));
		add(txtFieldSum);

	}

	public void doFinish() {
		document.setNumber(txtFieldNumber.getText().trim());
		document.setDate(datePicker.getDate());
		document.setUser(txtFieldUser.getText().trim());
		document.setSum(NumberUtils.parseDouble(txtFieldSum.getText()));
	}

	public T getDocument() {
		return document;
	}

	public void init(T document) {
		this.document = document;
		txtFieldNumber.setText(document.getNumber() == null ? "" : document
				.getNumber());

		datePicker.setDate(document.getDate() == null ? new Date() : document
				.getDate());

		txtFieldUser.setText(document.getUser() == null ? "" : document
				.getUser());

		txtFieldSum.setText(Double.toString(document.getSum()));
	}
}

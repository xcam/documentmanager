package ru.atol.document.manager.view.component.document;

import javax.swing.JLabel;
import javax.swing.JTextField;

import ru.atol.document.manager.model.entity.document.BillOfParcels;
import ru.atol.document.manager.utils.NumberUtils;
import ru.atol.document.manager.view.listener.DoubleDocumentListener;

public class BillOfParcelsPanel extends DocumentPanel<BillOfParcels> {

	private static final long serialVersionUID = -2434914175951342476L;
	private JTextField txtFieldCurrency;
	private JTextField txtFieldExchangeRate;
	private JTextField txtFieldArticle;
	private JTextField txtFieldQuantity;

	public BillOfParcelsPanel(BillOfParcels document) {
		super();
		init(document);
	}

	@Override
	protected void createFields() {
		super.createFields();

		txtFieldCurrency = new JTextField();
		txtFieldExchangeRate = new JTextField();
		txtFieldExchangeRate.getDocument().addDocumentListener(
				new DoubleDocumentListener(txtFieldExchangeRate));
		txtFieldQuantity = new JTextField();
		txtFieldQuantity.getDocument().addDocumentListener(
				new DoubleDocumentListener(txtFieldQuantity));
		txtFieldArticle = new JTextField();

		add(new JLabel("Валюта:"));
		add(txtFieldCurrency);
		add(new JLabel("Курс валюты:"));
		add(txtFieldExchangeRate);
		add(new JLabel("Товар:"));
		add(txtFieldArticle);
		add(new JLabel("Количество:"));
		add(txtFieldQuantity);
	}

	@Override
	public void init(BillOfParcels document) {
		super.init(document);

		txtFieldCurrency.setText(document.getCurrency() == null ? "" : document
				.getCurrency());
		txtFieldExchangeRate
				.setText(Double.toString(document.getExchangeRate()));
		txtFieldQuantity.setText(Double.toString(document.getQuantity()));
		txtFieldArticle.setText(document.getArticle() == null ? "" : document
				.getArticle());
	}

	@Override
	public void doFinish() {
		super.doFinish();
		document.setCurrency(txtFieldCurrency.getText().trim());
		document.setExchangeRate(NumberUtils.parseDouble(txtFieldExchangeRate
				.getText().trim()));
		document.setQuantity(NumberUtils.parseDouble(txtFieldQuantity.getText()
				.trim()));
		document.setArticle(txtFieldArticle.getText().trim());
	}
}

package ru.atol.document.manager.view.component;

import java.awt.Component;

import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellEditor;

public class ComponentCellEditor extends AbstractCellEditor implements
		TableCellEditor {
	private static final long serialVersionUID = 8592930908455314876L;

	@Override
	public Component getTableCellEditorComponent(JTable table, Object value,
			boolean isSelected, int row, int column) {
		if (value instanceof Component) {
			return (Component) value;
		}

		JTextField textField = new JTextField();
		if (value != null) {
			textField.setText(value.toString());
		}
		return textField;
	}

	@Override
	public Object getCellEditorValue() {
		return null;
	}

}

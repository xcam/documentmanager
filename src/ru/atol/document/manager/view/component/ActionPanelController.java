package ru.atol.document.manager.view.component;

import ru.atol.document.manager.model.entity.Entity;

public interface ActionPanelController<T extends Entity> {
	public void edit(T value);

	public void delete(T value);
}

package ru.atol.document.manager.view.listener;

public interface DataChangeListener {
	public void dataChanged();
}

package ru.atol.document.manager.view.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;

public class ExportActionListener implements ActionListener {

	private JFileChooser fileChooser;

	public ExportActionListener() {
		fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		int result = fileChooser.showSaveDialog(null);
		if (result == JFileChooser.APPROVE_OPTION) {
			File file = fileChooser.getSelectedFile();
			ExportTask exportTask = new ExportTask();
			exportTask.setFile(file);
			exportTask.execute();
		}
	}

}

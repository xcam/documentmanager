package ru.atol.document.manager.view.listener;

import java.io.File;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import ru.atol.document.manager.exporter.Exporter;
import ru.atol.document.manager.exporter.ExporterFactory;
import ru.atol.document.manager.exporter.ListOfDocument;
import ru.atol.document.manager.exporter.exception.ExporterException;
import ru.atol.document.manager.model.dao.DAOFactory;
import ru.atol.document.manager.model.dao.DocumentDAO;
import ru.atol.document.manager.model.entity.document.Document;
import ru.atol.document.manager.utils.LogUtils;
import ru.atol.document.manager.view.component.progressbar.Task;

public class ExportTask extends Task {

	private static Logger log = Logger.getLogger(ExportTask.class.getName());
	
	private File file;

	@Override
	protected void process() {

		Exporter exporter = ExporterFactory.getXmlExporter();
		DocumentDAO dao = DAOFactory.getDefaultDAOFactory().getDocumentDAO();
		int quantity = dao.quantity(null);
		List<Document> documents = dao.getData(0, quantity, null);
		quantity = documents.size();
		ListOfDocument listOfDocument = new ListOfDocument();

		int i = 0;
		for (Document document : documents) {
			i++;
			setProgress((int) (MAX_PROGRESS_VALUE * i / quantity));
			DAOFactory.getDefaultDAOFactory().updateDocument(document);
			listOfDocument.add(document);
		}
		try {
			exporter.export(listOfDocument, file);
			setProgress(MAX_PROGRESS_VALUE);
		} catch (ExporterException e) {
			log.log(Level.SEVERE, LogUtils.ERROR_MESSAGE, e);
			JOptionPane.showOptionDialog(null, "Неверный файл",
					"Ошибка сохранения", JOptionPane.OK_CANCEL_OPTION,
					JOptionPane.ERROR_MESSAGE, null, null, null);
			cancel(true);
		}

	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}
}

package ru.atol.document.manager.view.listener;

import java.awt.Color;

import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class IntegerDocumentListener implements DocumentListener {

	private JTextField textField;

	public IntegerDocumentListener(JTextField textField) {
		this.textField = textField;
	}

	public void changedUpdate(DocumentEvent e) {
		warn();
	}

	public void removeUpdate(DocumentEvent e) {
		warn();
	}

	public void insertUpdate(DocumentEvent e) {
		warn();
	}

	public void warn() {
		try {
			Integer.parseInt(textField.getText());
			textField.setBackground(UIManager.getColor("TextField.background"));
		} catch (Exception e) {
			textField.setBackground(Color.PINK);
		}
	}
}

package ru.atol.document.manager.view.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;

public class ImportActionListener implements ActionListener {

	private JFileChooser fileChooser;

	public ImportActionListener() {
		fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		int result = fileChooser.showOpenDialog(null);
		if (result == JFileChooser.APPROVE_OPTION) {
			File file = fileChooser.getSelectedFile();
			ImportTask importTask = new ImportTask();
			importTask.setFile(file);
			importTask.execute();
		}
	}
}

package ru.atol.document.manager.view.listener;

import java.io.File;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import ru.atol.document.manager.exporter.Exporter;
import ru.atol.document.manager.exporter.ExporterFactory;
import ru.atol.document.manager.exporter.ListOfDocument;
import ru.atol.document.manager.exporter.exception.ExporterException;
import ru.atol.document.manager.model.dao.DAOFactory;
import ru.atol.document.manager.model.dao.DocumentDAO;
import ru.atol.document.manager.model.entity.Entity;
import ru.atol.document.manager.model.entity.document.Document;
import ru.atol.document.manager.utils.LogUtils;
import ru.atol.document.manager.view.component.progressbar.Task;

public class ImportTask extends Task {

	private static Logger log = Logger.getLogger(ImportTask.class.getName());
	
	private File file;

	@Override
	protected void process() {

		Exporter exporter = ExporterFactory.getXmlExporter();
		ListOfDocument listOfDocument = null;
		try {
			listOfDocument = exporter.importFromFile(file);
		} catch (ExporterException e) {
			log.log(Level.SEVERE, LogUtils.ERROR_MESSAGE, e);
			JOptionPane.showOptionDialog(null, "Неверный файл",
					"Ошибка загрузки", JOptionPane.OK_CANCEL_OPTION,
					JOptionPane.ERROR_MESSAGE, null, null, null);
			cancel(true);
			return;
		}
		DocumentDAO dao = DAOFactory.getDefaultDAOFactory().getDocumentDAO();

		List<Document> documents = listOfDocument.getList();
		int quantity = documents.size();

		int i = 0;
		for (Document document : documents) {
			i++;
			setProgress((int) (MAX_PROGRESS_VALUE * i / quantity));
			document.setId(Entity.ID_NOT_SET);
			dao.saveOrUpdate(document);
		}
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}
}

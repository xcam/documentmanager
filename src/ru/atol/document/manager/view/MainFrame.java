package ru.atol.document.manager.view;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import ru.atol.document.manager.model.dao.DAOFactory;
import ru.atol.document.manager.model.entity.document.ApplicationForPayment;
import ru.atol.document.manager.model.entity.document.BillOfParcels;
import ru.atol.document.manager.model.entity.document.Document;
import ru.atol.document.manager.model.entity.document.PaymentDocument;
import ru.atol.document.manager.utils.GUIUtils;
import ru.atol.document.manager.utils.LogUtils;
import ru.atol.document.manager.view.component.ComponentCellEditor;
import ru.atol.document.manager.view.component.ComponentCellRenderer;
import ru.atol.document.manager.view.component.document.dialog.DocumentDialog;
import ru.atol.document.manager.view.component.pagingtable.PagingTable;
import ru.atol.document.manager.view.component.pagingtable.document.DocumentActionPanelController;
import ru.atol.document.manager.view.component.pagingtable.document.DocumentFilterPanel;
import ru.atol.document.manager.view.component.pagingtable.document.DocumentPagingTableModel;
import ru.atol.document.manager.view.listener.ExportActionListener;
import ru.atol.document.manager.view.listener.ImportActionListener;

public class MainFrame extends JFrame {

	private static final long serialVersionUID = 2001734490780920750L;

	private static final String LOGGING_PROPERTIES_PATH = "conf/logging.properties";

	private static Logger log = Logger.getLogger(MainFrame.class.getName());

	private static MainFrame current;

	private PagingTable<Document> pagingTable;

	public MainFrame() {
		super("Менеджер документов");
		current = this;
	}

	private void init() {
		JMenuBar menuBar = createMenu();
		setJMenuBar(menuBar);

		Container contentPane = getContentPane();

		DocumentPagingTableModel pagingTableModel = new DocumentPagingTableModel();

		JTable table = new JTable(pagingTableModel);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.getTableHeader().setReorderingAllowed(false);
		table.setRowHeight(40);

		ComponentCellRenderer componentCellRenderer = new ComponentCellRenderer();
		ComponentCellEditor componentCellEditor = new ComponentCellEditor();
		TableColumnModel columnModel = table.getColumnModel();
		for (int i = 0; i < columnModel.getColumnCount(); i++) {
			TableColumn column = columnModel.getColumn(i);
			column.setCellRenderer(componentCellRenderer);
			column.setCellEditor(componentCellEditor);
		}

		pagingTable = new PagingTable<Document>(pagingTableModel, table,
				DAOFactory.getDefaultDAOFactory().getDocumentDAO(),
				new DocumentFilterPanel());
		pagingTableModel
				.setActionPanelController(new DocumentActionPanelController());
		refresh();

		contentPane.add(pagingTable);
	}

	private JMenuBar createMenu() {
		JMenuBar menuBar = new JMenuBar();
		JMenu fileMenu = new JMenu("Файл");

		JMenu newFileMenuItem = createNewFileMenuItem();

		JMenuItem exportMenuItem = new JMenuItem("Сохранить");
		JMenuItem importMenuItem = new JMenuItem("Загрузить");
		exportMenuItem.addActionListener(new ExportActionListener());
		importMenuItem.addActionListener(new ImportActionListener());

		fileMenu.add(newFileMenuItem);
		fileMenu.addSeparator();
		fileMenu.add(exportMenuItem);
		fileMenu.add(importMenuItem);

		menuBar.add(fileMenu);
		return menuBar;
	}

	private JMenu createNewFileMenuItem() {
		JMenu newFileMenuItem = new JMenu("Новый документ");
		JMenuItem newBillOfParcelsMenuItem = new JMenuItem("Накладная");
		JMenuItem newPaymentDocumentMenuItem = new JMenuItem(
				"Платежный документ");
		JMenuItem newApplicationForPaymentMenuItem = new JMenuItem(
				"Заявка на оплату");

		newFileMenuItem.add(newBillOfParcelsMenuItem);
		newFileMenuItem.add(newPaymentDocumentMenuItem);
		newFileMenuItem.add(newApplicationForPaymentMenuItem);

		newBillOfParcelsMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				DocumentDialog<BillOfParcels> documentDialog = DocumentDialog
						.createDialog(new BillOfParcels(), "Создать");
				documentDialog.show();
			}
		});

		newApplicationForPaymentMenuItem
				.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						DocumentDialog<ApplicationForPayment> documentDialog = DocumentDialog
								.createDialog(new ApplicationForPayment(),
										"Создать");
						documentDialog.show();
					}
				});

		newPaymentDocumentMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				DocumentDialog<PaymentDocument> documentDialog = DocumentDialog
						.createDialog(new PaymentDocument(), "Создать");
				documentDialog.show();
			}
		});
		return newFileMenuItem;
	}

	public static void refresh() {
		current.pagingTable.getPagingTableController().refresh();
	}

	public static MainFrame getCurrent() {
		return current;
	}

	public static void main(String[] args) {

		try {
			LogManager.getLogManager().readConfiguration(
					new FileInputStream(new File(LOGGING_PROPERTIES_PATH)));
		} catch (Exception e) {
			log.log(Level.SEVERE, LogUtils.ERROR_MESSAGE, e);
		}
		MainFrame frame = new MainFrame();
		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setPreferredSize(new Dimension(screenSize.width / 2,
				screenSize.height / 2));

		frame.init();
		frame.pack();
		GUIUtils.moveToCenter(frame);
		frame.setVisible(true);
	}
}

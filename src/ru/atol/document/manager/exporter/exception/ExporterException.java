package ru.atol.document.manager.exporter.exception;

public class ExporterException extends Exception {
	private static final long serialVersionUID = -6789790516140670223L;

	public ExporterException(String message, Exception cause) {
		super(message, cause);
	}
}

package ru.atol.document.manager.exporter;

public class ExporterFactory {
	private static Exporter xmlExporter;

	public static Exporter getXmlExporter() {
		if (xmlExporter == null) {
			xmlExporter = new XMLExporter();
		}

		return xmlExporter;
	}
}

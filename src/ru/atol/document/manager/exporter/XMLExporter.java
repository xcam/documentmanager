package ru.atol.document.manager.exporter;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import ru.atol.document.manager.exporter.exception.ExporterException;
import ru.atol.document.manager.utils.LogUtils;

public class XMLExporter implements Exporter {

	private static Logger log = Logger.getLogger(XMLExporter.class.getName());
	
	@Override
	public void export(ListOfDocument listOfDocument, File file)
			throws ExporterException {
		if (file == null || listOfDocument == null) {
			return;
		}
		try {
			JAXBContext jaxbContext = JAXBContext
					.newInstance(ListOfDocument.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(listOfDocument, file);
		} catch (JAXBException e) {
			log.log(Level.SEVERE, LogUtils.ERROR_MESSAGE, e);
			throw new ExporterException("Ошибка экспорта.", e);
		}
	}

	@Override
	public ListOfDocument importFromFile(File file) throws ExporterException {
		try {
			JAXBContext jaxbContext = JAXBContext
					.newInstance(ListOfDocument.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			return (ListOfDocument) jaxbUnmarshaller.unmarshal(file);
		} catch (JAXBException e) {
			log.log(Level.SEVERE, LogUtils.ERROR_MESSAGE, e);
			throw new ExporterException("Ошибка импорта.", e);
		}
	}
}

package ru.atol.document.manager.exporter;

import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ru.atol.document.manager.model.entity.document.Document;

@XmlRootElement
public class ListOfDocument {

	private List<Document> list = new LinkedList<>();

	public void add(Document document) {
		list.add(document);
	}

	public List<Document> getList() {
		return list;
	}

	@XmlElement
	public void setList(List<Document> list) {
		this.list = list;
	}
}

package ru.atol.document.manager.exporter;

import java.io.File;

import ru.atol.document.manager.exporter.exception.ExporterException;

public interface Exporter {
	public void export(ListOfDocument listOfDocument, File file)
			throws ExporterException;

	public ListOfDocument importFromFile(File file) throws ExporterException;
}

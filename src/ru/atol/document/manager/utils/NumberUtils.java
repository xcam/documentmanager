package ru.atol.document.manager.utils;

public class NumberUtils {
	public static double parseDouble(String string) {
		try {
			return Double.parseDouble(string);
		} catch (Exception e) {
			return 0;
		}
	}

	public static int parseInt(String string) {
		try {
			return Integer.parseInt(string);
		} catch (Exception e) {
			return 0;
		}
	}
}

package ru.atol.document.manager.utils;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;

public class GUIUtils {
	public static void moveToCenter(Component component) {
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		component.setLocation(dim.width / 2 - component.getSize().width / 2,
				dim.height / 2 - component.getSize().height / 2);
	}
}

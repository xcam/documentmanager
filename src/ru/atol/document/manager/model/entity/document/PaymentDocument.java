package ru.atol.document.manager.model.entity.document;

import javax.xml.bind.annotation.XmlElement;

public class PaymentDocument extends Document {
	private String employee;

	public void copyFrom(PaymentDocument entity) {
		if (entity == null) {
			return;
		}

		super.copyFrom(entity);
		employee = entity.getEmployee();
	}

	public String getEmployee() {
		return employee;
	}

	@XmlElement
	public void setEmployee(String employee) {
		this.employee = employee;
	}
}

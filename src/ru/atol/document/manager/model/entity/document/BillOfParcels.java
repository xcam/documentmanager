package ru.atol.document.manager.model.entity.document;

import com.sun.xml.internal.txw2.annotation.XmlElement;

public class BillOfParcels extends Document {

	private String currency;
	private double exchangeRate;
	private String article;
	private double quantity;

	public void copyFrom(BillOfParcels entity) {
		if (entity == null) {
			return;
		}

		super.copyFrom(entity);
		currency = entity.getCurrency();
		exchangeRate = entity.getExchangeRate();
		article = entity.getArticle();
		quantity = entity.getQuantity();
	}

	public String getCurrency() {
		return currency;
	}

	@XmlElement
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public double getExchangeRate() {
		return exchangeRate;
	}

	@XmlElement
	public void setExchangeRate(double exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public String getArticle() {
		return article;
	}

	@XmlElement
	public void setArticle(String article) {
		this.article = article;
	}

	public double getQuantity() {
		return quantity;
	}

	@XmlElement
	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}
}

package ru.atol.document.manager.model.entity.document;

import com.sun.xml.internal.txw2.annotation.XmlElement;

public class ApplicationForPayment extends Document {

	private String contractor;
	private String currency;
	private double exchangeRate;
	private double fee;

	public void copyFrom(ApplicationForPayment entity) {
		if (entity == null) {
			return;
		}

		super.copyFrom(entity);
		currency = entity.getCurrency();
		exchangeRate = entity.getExchangeRate();
		contractor = entity.getContractor();
		fee = entity.getFee();
	}

	public String getContractor() {
		return contractor;
	}

	@XmlElement
	public void setContractor(String contractor) {
		this.contractor = contractor;
	}

	public String getCurrency() {
		return currency;
	}

	@XmlElement
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public double getExchangeRate() {
		return exchangeRate;
	}

	@XmlElement
	public void setExchangeRate(double exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public double getFee() {
		return fee;
	}

	@XmlElement
	public void setFee(double fee) {
		this.fee = fee;
	}
}

package ru.atol.document.manager.model.entity.document;

import java.util.Date;

import javax.xml.bind.annotation.XmlSeeAlso;

import ru.atol.document.manager.model.entity.Entity;

import com.sun.xml.internal.txw2.annotation.XmlElement;

@XmlSeeAlso({ BillOfParcels.class, ApplicationForPayment.class,
		PaymentDocument.class })
public abstract class Document extends Entity {
	private String number;
	private Date date;
	private String user;
	private double sum;

	public void copyFrom(Document entity) {
		if (entity == null) {
			return;
		}

		number = entity.getNumber();
		date = entity.getDate();
		user = entity.getUser();
		sum = entity.getSum();
	}

	public String getNumber() {
		return number;
	}

	@XmlElement
	public void setNumber(String number) {
		this.number = number;
	}

	public Date getDate() {
		return date;
	}

	@XmlElement
	public void setDate(Date date) {
		this.date = date;
	}

	public String getUser() {
		return user;
	}

	@XmlElement
	public void setUser(String user) {
		this.user = user;
	}

	public double getSum() {
		return sum;
	}

	@XmlElement
	public void setSum(double sum) {
		this.sum = sum;
	}

}

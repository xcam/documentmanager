package ru.atol.document.manager.model.entity;

public abstract class Entity {
	public static final long ID_NOT_SET = -1;

	private long id = ID_NOT_SET;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
}

package ru.atol.document.manager.model.dao.mysql;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import ru.atol.document.manager.model.dao.Crud;
import ru.atol.document.manager.model.dao.DAOFactory;
import ru.atol.document.manager.model.dao.DocumentDAO;
import ru.atol.document.manager.model.entity.document.ApplicationForPayment;
import ru.atol.document.manager.model.entity.document.BillOfParcels;
import ru.atol.document.manager.model.entity.document.PaymentDocument;
import ru.atol.document.manager.utils.LogUtils;

public class MysqlDAOFactory extends DAOFactory {

	private static final String DB_PROPERTIES_PATH = "conf/db.properties";

	private static final String PROP_DATA_URL = "Database.DataURL";
	private static final String PROP_USER = "Database.Prop.user";
	private static final String PROP_PASSWORD = "Database.Prop.password";
	private static final String PROP_DATABASE_DRIVER = "Database.Driver";
	
	private static Logger log = Logger.getLogger(MysqlDAOFactory.class.getName());

	private static Properties properties;
	private static Connection connection;

	private DocumentDAO documentDAO;
	private Crud<BillOfParcels> billOfParcelsDAO;
	private Crud<ApplicationForPayment> applicationForPaymentDAO;
	private Crud<PaymentDocument> paymentDocumentDAO;

	private static Properties getDBProperties() {
		if (properties == null) {
			properties = new Properties();
			try {
				properties.load(new FileInputStream(
						new File(DB_PROPERTIES_PATH)));
			} catch (Exception e) {
				log.log(Level.SEVERE, LogUtils.ERROR_MESSAGE, e);
				JOptionPane.showMessageDialog(null,
						"Ошибка загрузки настройки соединения с базой данных",
						"Ошибка", JOptionPane.ERROR_MESSAGE);
			}
		}
		return properties;
	}

	static Connection getConnection() {
		try {

			if (connection == null) {
				Properties prop = getDBProperties();
				Class.forName(prop.getProperty(PROP_DATABASE_DRIVER,
						"com.mysql.jdbc.Driver"));
				String url = prop.getProperty(PROP_DATA_URL,
						"jdbc:mysql://localhost/documents");
				url += "?autoReconnect=true&useUnicode=true&characterEncoding=utf8";
				connection = DriverManager.getConnection(url,
						prop.getProperty(PROP_USER, "root"),
						prop.getProperty(PROP_PASSWORD, "root"));
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, LogUtils.ERROR_MESSAGE, e);
			JOptionPane.showMessageDialog(null, e.getMessage(),
					"Ошибка соединения с базой данных",
					JOptionPane.ERROR_MESSAGE, null);
			System.exit(-1);
		}
		return connection;
	}

	@Override
	public DocumentDAO getDocumentDAO() {
		if (documentDAO == null) {
			documentDAO = new MysqlDocumentDAO();
		}
		return documentDAO;
	}

	@Override
	public Crud<BillOfParcels> getBillOfParcelsDAO() {
		if (billOfParcelsDAO == null) {
			billOfParcelsDAO = new BillOfParcelsDAO();
		}
		return billOfParcelsDAO;
	}

	@Override
	public Crud<ApplicationForPayment> getApplicationForPaymentDAO() {
		if (applicationForPaymentDAO == null) {
			applicationForPaymentDAO = new ApplicationForPaymentDAO();
		}
		return applicationForPaymentDAO;
	}

	@Override
	public Crud<PaymentDocument> getPaymentDocumentDao() {
		if (paymentDocumentDAO == null) {
			paymentDocumentDAO = new PaymentDocumentDAO();
		}
		return paymentDocumentDAO;
	}
}

package ru.atol.document.manager.model.dao.mysql;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import ru.atol.document.manager.model.dao.filter.Criteria;
import ru.atol.document.manager.model.dao.filter.DateCriteria;
import ru.atol.document.manager.model.dao.filter.StringCriteria;

public class MysqlUtils {
	public static String appendWhere(String sql, List<String> expressions) {
		if (expressions == null || expressions.size() == 0) {
			return sql;
		}

		StringBuilder builder = new StringBuilder();
		builder.append(sql);
		builder.append(" WHERE ");
		Iterator<String> it = expressions.iterator();
		if (it.hasNext()) {
			builder.append(it.next());
		}
		while (it.hasNext()) {
			builder.append(" AND ");
			builder.append(it.next());
		}

		return builder.toString();
	}

	public static String appendLimit(String sql) {
		if (sql == null) {
			return sql;
		}
		return sql + " LIMIT ?, ?";
	}

	public static String generateStringForCriteria(String column,
			Criteria criteria) {
		if (criteria == null) {
			return null;
		}

		if (criteria instanceof StringCriteria) {
			StringCriteria stringCriteria = (StringCriteria) criteria;
			if (stringCriteria != null && stringCriteria.isValid()) {
				return column + " LIKE ?";
			}
		} else if (criteria instanceof DateCriteria) {
			DateCriteria dateCriteria = (DateCriteria) criteria;
			if (dateCriteria != null && dateCriteria.isValid()) {
				return column + " BETWEEN ? AND ?";
			}
		}

		return null;
	}

	public static int setCriteriaDataToStatement(PreparedStatement statement,
			int offset, Criteria criteria) throws SQLException {

		if (criteria instanceof StringCriteria) {
			StringCriteria stringCriteria = (StringCriteria) criteria;
			if (stringCriteria.isValid()) {
				statement.setString(offset++, stringCriteria.getLike());
			}
		} else if (criteria instanceof DateCriteria) {
			DateCriteria dateCriteria = (DateCriteria) criteria;
			if (dateCriteria.isValid()) {
				statement.setDate(offset++, new Date(dateCriteria.getAfter()
						.getTime()));
				statement.setDate(offset++, new Date(dateCriteria.getBefore()
						.getTime()));
			}
		}

		return offset;
	}
}

package ru.atol.document.manager.model.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import ru.atol.document.manager.model.dao.DocumentDAO;
import ru.atol.document.manager.model.dao.PageableDAO;
import ru.atol.document.manager.model.dao.filter.DocumentFilter;
import ru.atol.document.manager.model.dao.filter.Filter;
import ru.atol.document.manager.model.entity.document.Document;

public class MysqlDocumentDAO extends DocumentCrudDAO<Document> implements
		DocumentDAO, PageableDAO<Document> {

	private static final String SQL_GET_DATA = "SELECT * FROM document";
	private static final String SQL_GET_QUANTITY = "SELECT COUNT(*) FROM document";

	@Override
	public List<Document> getData(int offset, int size, Filter filter) {
		List<Document> list = new LinkedList<>();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			Connection conn = MysqlDAOFactory.getConnection();
			String sql = MysqlUtils.appendWhere(SQL_GET_DATA,
					convertFilter(filter));
			sql = MysqlUtils.appendLimit(sql);
			preparedStatement = conn.prepareStatement(sql);
			int parameterIndex = 1;
			parameterIndex = setFilterValuesToStatement(preparedStatement,
					parameterIndex, filter);

			preparedStatement.setInt(parameterIndex++, offset);
			preparedStatement.setInt(parameterIndex++, size);
			if (preparedStatement.execute()) {
				resultSet = preparedStatement.getResultSet();
				while (resultSet.next()) {
					Document document = createDocumentFromResultSet(resultSet);
					if (document != null) {
						list.add(document);
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			JDBCUtils.closeResultSet(resultSet);
			JDBCUtils.closeStatement(preparedStatement);
		}
		return list;
	}

	@Override
	public int quantity(Filter filter) {
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			Connection conn = MysqlDAOFactory.getConnection();

			String sql = MysqlUtils.appendWhere(SQL_GET_QUANTITY,
					convertFilter(filter));
			statement = conn.prepareStatement(sql);

			int offset = 1;
			offset = setFilterValuesToStatement(statement, offset, filter);

			if (statement.execute()) {
				resultSet = statement.getResultSet();
				if (resultSet.next()) {
					return resultSet.getInt(1);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			JDBCUtils.closeResultSet(resultSet);
			JDBCUtils.closeStatement(statement);
		}
		return 0;
	}

	private int setFilterValuesToStatement(PreparedStatement statement,
			int offset, Filter filter) throws SQLException {
		if (filter instanceof DocumentFilter) {
			DocumentFilter documentFilter = (DocumentFilter) filter;
			offset = MysqlUtils.setCriteriaDataToStatement(statement, offset,
					documentFilter.getNumberCriteria());
			offset = MysqlUtils.setCriteriaDataToStatement(statement, offset,
					documentFilter.getDateCriteria());
		}
		return offset;
	}

	@Override
	public Document getByID(long id) {
		return super.getDocumentByID(id);
	}

	@Override
	public void update(Document entity) {
		if (entity == null) {
			return;
		}
		Document document = super.getDocumentByID(entity.getId());
		entity.copyFrom(document);
	}

	private List<String> convertFilter(Filter filter) {
		List<String> list = new LinkedList<>();
		if (filter instanceof DocumentFilter) {
			DocumentFilter documentFilter = (DocumentFilter) filter;
			String criteria = MysqlUtils.generateStringForCriteria(
					DocumentCrudDAO.COLUMN_NUMBER,
					documentFilter.getNumberCriteria());
			if (criteria != null) {
				list.add(criteria);
			}
			criteria = MysqlUtils.generateStringForCriteria(
					DocumentCrudDAO.COLUMN_DATE,
					documentFilter.getDateCriteria());
			if (criteria != null) {
				list.add(criteria);
			}
		}

		return list;
	}
}

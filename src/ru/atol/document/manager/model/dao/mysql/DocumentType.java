package ru.atol.document.manager.model.dao.mysql;

import ru.atol.document.manager.model.entity.document.ApplicationForPayment;
import ru.atol.document.manager.model.entity.document.BillOfParcels;
import ru.atol.document.manager.model.entity.document.PaymentDocument;

public enum DocumentType {
	BILL_OF_PARCELS, PAYMENT_DOCUMENT, APPLICATION_FOR_PAYMENT;

	@SuppressWarnings("rawtypes")
	public static DocumentType valueOf(Class clazz) {
		if (clazz.equals(BillOfParcels.class)) {
			return BILL_OF_PARCELS;
		}

		if (clazz.equals(PaymentDocument.class)) {
			return PAYMENT_DOCUMENT;
		}

		if (clazz.equals(ApplicationForPayment.class)) {
			return APPLICATION_FOR_PAYMENT;
		}

		return null;
	}
}

package ru.atol.document.manager.model.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import ru.atol.document.manager.model.entity.Entity;
import ru.atol.document.manager.model.entity.document.BillOfParcels;
import ru.atol.document.manager.model.entity.document.Document;

public class BillOfParcelsDAO extends DocumentCrudDAO<BillOfParcels> {

	protected static final String COLUMN_ID = "ID";
	protected static final String COLUMN_CURRENCY = "CURRENCY";
	protected static final String COLUMN_EXCHANGE_RATE = "EXCHANGE_RATE";
	protected static final String COLUMN_ARTICLE = "ARTICLE";
	protected static final String COLUMN_QUANTITY = "QUANTITY";

	private static final String SQL_INSERT_OR_UPDATE_DOCUMENT = "INSERT INTO bill_of_parcels (ID, CURRENCY, EXCHANGE_RATE, ARTICLE, QUANTITY) VALUES(?,?,?,?,?) "
			+ "on duplicate key update ID=values(ID), CURRENCY=values(CURRENCY), EXCHANGE_RATE=values(EXCHANGE_RATE), ARTICLE=values(ARTICLE), QUANTITY=values(QUANTITY)";
	private static final String SQL_GET_BY_ID = "SELECT * FROM bill_of_parcels WHERE ID=?";

	@Override
	public void saveOrUpdate(BillOfParcels entity) {
		if (entity == null) {
			return;
		}

		int offset = 1;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Connection conn = null;
		try {
			conn = MysqlDAOFactory.getConnection();
			conn.setAutoCommit(false);
			// save or update document
			super.saveOrUpdate(entity);

			if (entity.getId() == Entity.ID_NOT_SET) {
				JDBCUtils.rollback(conn);
				return;
			}

			preparedStatement = conn
					.prepareStatement(SQL_INSERT_OR_UPDATE_DOCUMENT);
			preparedStatement.setLong(offset++, entity.getId());
			preparedStatement.setString(offset++, entity.getCurrency());
			preparedStatement.setDouble(offset++, entity.getExchangeRate());
			preparedStatement.setString(offset++, entity.getArticle());
			preparedStatement.setDouble(offset++, entity.getQuantity());

			preparedStatement.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			JDBCUtils.rollback(conn);
			e.printStackTrace();
		} finally {
			JDBCUtils.enableAutoCommit(conn);
			JDBCUtils.closeResultSet(resultSet);
			JDBCUtils.closeStatement(preparedStatement);
		}
	}

	@Override
	public BillOfParcels getByID(long id) {
		Document document = super.getDocumentByID(id);
		if (!(document instanceof BillOfParcels)) {
			return null;
		}

		BillOfParcels billOfParcels = (BillOfParcels) document;

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			Connection conn = MysqlDAOFactory.getConnection();
			preparedStatement = conn.prepareStatement(SQL_GET_BY_ID);
			preparedStatement.setLong(1, id);
			if (preparedStatement.execute()) {
				resultSet = preparedStatement.getResultSet();
				if (resultSet.next()) {
					return updateDocument(resultSet, billOfParcels);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			JDBCUtils.closeResultSet(resultSet);
			JDBCUtils.closeStatement(preparedStatement);
		}
		return billOfParcels;
	}

	private BillOfParcels updateDocument(ResultSet resultSet,
			BillOfParcels document) throws SQLException {

		document.setCurrency(resultSet.getString(COLUMN_CURRENCY));
		document.setExchangeRate(resultSet.getDouble(COLUMN_EXCHANGE_RATE));
		document.setArticle(resultSet.getString(COLUMN_ARTICLE));
		document.setQuantity(resultSet.getDouble(COLUMN_QUANTITY));
		return document;
	}

	@Override
	public void update(BillOfParcels entity) {
		if (entity == null) {
			return;
		}
		BillOfParcels entityFromDB = getByID(entity.getId());
		entity.copyFrom(entityFromDB);
	}
}

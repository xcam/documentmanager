package ru.atol.document.manager.model.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import ru.atol.document.manager.model.entity.Entity;
import ru.atol.document.manager.model.entity.document.ApplicationForPayment;
import ru.atol.document.manager.model.entity.document.Document;

public class ApplicationForPaymentDAO extends
		DocumentCrudDAO<ApplicationForPayment> {
	protected static final String COLUMN_ID = "ID";
	protected static final String COLUMN_CONTRACTOR = "CONTRACTOR";
	protected static final String COLUMN_CURRENCY = "CURRENCY";
	protected static final String COLUMN_EXCHANGE_RATE = "EXCHANGE_RATE";
	protected static final String COLUMN_FEE = "FEE";

	private static final String SQL_INSERT_OR_UPDATE_DOCUMENT = "INSERT INTO application_for_payment (ID, CONTRACTOR, CURRENCY, EXCHANGE_RATE, FEE) VALUES(?,?,?,?,?) "
			+ "on duplicate key update ID=values(ID), CONTRACTOR=values(CONTRACTOR), CURRENCY=values(CURRENCY), EXCHANGE_RATE=values(EXCHANGE_RATE), FEE=values(FEE)";
	private static final String SQL_GET_BY_ID = "SELECT * FROM application_for_payment WHERE ID=?";

	@Override
	public void saveOrUpdate(ApplicationForPayment entity) {
		if (entity == null) {
			return;
		}

		int offset = 1;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Connection conn = null;
		try {
			conn = MysqlDAOFactory.getConnection();
			conn.setAutoCommit(false);
			// save or update document
			super.saveOrUpdate(entity);

			if (entity.getId() == Entity.ID_NOT_SET) {
				JDBCUtils.rollback(conn);
				return;
			}

			preparedStatement = conn
					.prepareStatement(SQL_INSERT_OR_UPDATE_DOCUMENT);
			preparedStatement.setLong(offset++, entity.getId());
			preparedStatement.setString(offset++, entity.getContractor());
			preparedStatement.setString(offset++, entity.getCurrency());
			preparedStatement.setDouble(offset++, entity.getExchangeRate());
			preparedStatement.setDouble(offset++, entity.getFee());

			preparedStatement.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			JDBCUtils.rollback(conn);
			e.printStackTrace();
		} finally {
			JDBCUtils.enableAutoCommit(conn);
			JDBCUtils.closeResultSet(resultSet);
			JDBCUtils.closeStatement(preparedStatement);
		}
	}

	@Override
	public ApplicationForPayment getByID(long id) {
		Document document = super.getDocumentByID(id);
		if (!(document instanceof ApplicationForPayment)) {
			return null;
		}

		ApplicationForPayment applicationForPayment = (ApplicationForPayment) document;

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			Connection conn = MysqlDAOFactory.getConnection();
			preparedStatement = conn.prepareStatement(SQL_GET_BY_ID);
			preparedStatement.setLong(1, id);
			if (preparedStatement.execute()) {
				resultSet = preparedStatement.getResultSet();
				if (resultSet.next()) {
					return updateDocument(resultSet, applicationForPayment);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			JDBCUtils.closeResultSet(resultSet);
			JDBCUtils.closeStatement(preparedStatement);
		}

		return applicationForPayment;
	}

	private ApplicationForPayment updateDocument(ResultSet resultSet,
			ApplicationForPayment document) throws SQLException {
		document.setContractor(resultSet.getString(COLUMN_CONTRACTOR));
		document.setCurrency(resultSet.getString(COLUMN_CURRENCY));
		document.setExchangeRate(resultSet.getDouble(COLUMN_EXCHANGE_RATE));
		document.setFee(resultSet.getDouble(COLUMN_FEE));
		return document;
	}

	@Override
	public void update(ApplicationForPayment entity) {
		if (entity == null) {
			return;
		}
		ApplicationForPayment entityFromDB = getByID(entity.getId());
		entity.copyFrom(entityFromDB);
	}
}

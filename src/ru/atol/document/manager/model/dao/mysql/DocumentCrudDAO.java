package ru.atol.document.manager.model.dao.mysql;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import ru.atol.document.manager.model.dao.Crud;
import ru.atol.document.manager.model.entity.Entity;
import ru.atol.document.manager.model.entity.document.ApplicationForPayment;
import ru.atol.document.manager.model.entity.document.BillOfParcels;
import ru.atol.document.manager.model.entity.document.Document;
import ru.atol.document.manager.model.entity.document.PaymentDocument;

public abstract class DocumentCrudDAO<T extends Document> implements Crud<T> {
	protected static final String COLUMN_ID = "ID";
	protected static final String COLUMN_NUMBER = "NUMBER";
	protected static final String COLUMN_USER = "USER";
	protected static final String COLUMN_SUM = "SUM";
	protected static final String COLUMN_DATE = "DATE";
	protected static final String COLUMN_TYPE = "TYPE";

	private static final String SQL_DELETE_DOCUMENT = "DELETE FROM document WHERE ID=?";
	private static final String SQL_UPDATE_DOCUMENT = "UPDATE document SET NUMBER=?, USER=?, SUM=?, DATE=?, TYPE=? WHERE ID=?";
	private static final String SQL_INSERT_DOCUMENT = "INSERT INTO document (NUMBER, USER, SUM, DATE, TYPE) VALUES(?,?,?,?,?)";
	private static final String SQL_GET_BY_ID = "SELECT * FROM document WHERE ID=?";

	@Override
	public void delete(long id) {
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Connection conn = null;
		try {
			conn = MysqlDAOFactory.getConnection();
			conn.setAutoCommit(false);
			preparedStatement = conn.prepareStatement(SQL_DELETE_DOCUMENT);
			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			JDBCUtils.rollback(conn);
			e.printStackTrace();
		} finally {
			JDBCUtils.enableAutoCommit(conn);
			JDBCUtils.closeResultSet(resultSet);
			JDBCUtils.closeStatement(preparedStatement);
		}
	}

	@Override
	public void delete(T entity) {
		if (entity == null) {
			return;
		}

		delete(entity.getId());
	}

	@Override
	public void saveOrUpdate(T entity) {
		if (entity == null) {
			return;
		}

		boolean isInsert = entity.getId() == Entity.ID_NOT_SET;
		int offset = 1;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Connection conn = null;
		try {
			conn = MysqlDAOFactory.getConnection();
			if (isInsert) {
				preparedStatement = conn.prepareStatement(SQL_INSERT_DOCUMENT,
						Statement.RETURN_GENERATED_KEYS);
			} else {
				preparedStatement = conn.prepareStatement(SQL_UPDATE_DOCUMENT);
			}
			preparedStatement.setString(offset++, entity.getNumber());
			preparedStatement.setString(offset++, entity.getUser());
			preparedStatement.setDouble(offset++, entity.getSum());
			preparedStatement.setDate(offset++, new Date(entity.getDate()
					.getTime()));
			preparedStatement.setString(offset++,
					DocumentType.valueOf(entity.getClass()).toString());

			if (!isInsert) {
				preparedStatement.setLong(offset++, entity.getId());
			}

			int result = preparedStatement.executeUpdate();
			if (isInsert && result == 1) {
				resultSet = preparedStatement.getGeneratedKeys();
				if (resultSet.next()) {
					entity.setId(resultSet.getLong(1));
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			JDBCUtils.closeResultSet(resultSet);
			JDBCUtils.closeStatement(preparedStatement);
		}
	}

	protected Document getDocumentByID(long id) {
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			Connection conn = MysqlDAOFactory.getConnection();
			preparedStatement = conn.prepareStatement(SQL_GET_BY_ID);
			preparedStatement.setLong(1, id);
			if (preparedStatement.execute()) {
				resultSet = preparedStatement.getResultSet();
				if (resultSet.next()) {
					return createDocumentFromResultSet(resultSet);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			JDBCUtils.closeResultSet(resultSet);
			JDBCUtils.closeStatement(preparedStatement);
		}
		return null;
	}

	protected Document createDocumentFromResultSet(ResultSet resultSet)
			throws SQLException {
		String type = resultSet.getString(COLUMN_TYPE);
		Document document;
		switch (DocumentType.valueOf(type)) {
		case APPLICATION_FOR_PAYMENT: {
			document = new ApplicationForPayment();
			break;
		}
		case BILL_OF_PARCELS: {
			document = new BillOfParcels();
			break;
		}
		case PAYMENT_DOCUMENT: {
			document = new PaymentDocument();
			break;
		}
		default:
			return null;
		}

		document.setId(resultSet.getLong(COLUMN_ID));
		document.setDate(resultSet.getDate(COLUMN_DATE));
		document.setNumber(resultSet.getString(COLUMN_NUMBER));
		document.setSum(resultSet.getDouble(COLUMN_SUM));
		document.setUser(resultSet.getString(COLUMN_USER));

		return document;
	}
}

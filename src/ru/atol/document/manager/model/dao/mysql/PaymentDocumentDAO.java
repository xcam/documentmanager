package ru.atol.document.manager.model.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import ru.atol.document.manager.model.entity.Entity;
import ru.atol.document.manager.model.entity.document.Document;
import ru.atol.document.manager.model.entity.document.PaymentDocument;

public class PaymentDocumentDAO extends DocumentCrudDAO<PaymentDocument> {
	protected static final String COLUMN_ID = "ID";
	protected static final String COLUMN_EMPLOYEE = "EMPLOYEE";

	private static final String SQL_INSERT_OR_UPDATE_DOCUMENT = "INSERT INTO payment_document (ID, EMPLOYEE) VALUES(?,?) "
			+ "on duplicate key update ID=values(ID), EMPLOYEE=values(EMPLOYEE)";

	private static final String SQL_GET_BY_ID = "SELECT * FROM payment_document WHERE ID=?";

	@Override
	public void saveOrUpdate(PaymentDocument entity) {
		if (entity == null) {
			return;
		}

		int offset = 1;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Connection conn = null;
		try {
			conn = MysqlDAOFactory.getConnection();
			conn.setAutoCommit(false);
			// save or update document
			super.saveOrUpdate(entity);

			if (entity.getId() == Entity.ID_NOT_SET) {
				JDBCUtils.rollback(conn);
				return;
			}

			preparedStatement = conn
					.prepareStatement(SQL_INSERT_OR_UPDATE_DOCUMENT);
			preparedStatement.setLong(offset++, entity.getId());
			preparedStatement.setString(offset++, entity.getEmployee());

			preparedStatement.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			JDBCUtils.rollback(conn);
			e.printStackTrace();
		} finally {
			JDBCUtils.enableAutoCommit(conn);
			JDBCUtils.closeResultSet(resultSet);
			JDBCUtils.closeStatement(preparedStatement);
		}
	}

	@Override
	public PaymentDocument getByID(long id) {
		Document document = super.getDocumentByID(id);
		if (!(document instanceof PaymentDocument)) {
			return null;
		}

		PaymentDocument paymentDocument = (PaymentDocument) document;

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			Connection conn = MysqlDAOFactory.getConnection();
			preparedStatement = conn.prepareStatement(SQL_GET_BY_ID);
			preparedStatement.setLong(1, id);
			if (preparedStatement.execute()) {
				resultSet = preparedStatement.getResultSet();
				if (resultSet.next()) {
					return updateDocument(resultSet, paymentDocument);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			JDBCUtils.closeResultSet(resultSet);
			JDBCUtils.closeStatement(preparedStatement);
		}

		return paymentDocument;
	}

	private PaymentDocument updateDocument(ResultSet resultSet,
			PaymentDocument document) throws SQLException {

		document.setEmployee(resultSet.getString(COLUMN_EMPLOYEE));
		return document;
	}

	@Override
	public void update(PaymentDocument entity) {
		if (entity == null) {
			return;
		}
		PaymentDocument entityFromDB = getByID(entity.getId());
		entity.copyFrom(entityFromDB);
	}
}

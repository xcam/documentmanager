package ru.atol.document.manager.model.dao;

import java.util.List;

import ru.atol.document.manager.model.dao.filter.Filter;
import ru.atol.document.manager.model.entity.Entity;

public interface PageableDAO<T extends Entity> {
	public List<T> getData(int offset, int size, Filter filter);

	public int quantity(Filter filter);
}

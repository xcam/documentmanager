package ru.atol.document.manager.model.dao.filter;

public class StringCriteria extends Criteria {

	private String like;

	@Override
	public boolean isValid() {
		return getLike() != null && !getLike().isEmpty();
	}

	public StringCriteria(String like) {
		this.like = like;
	}

	public String getLike() {
		return like;
	}

	public void setLike(String like) {
		this.like = like;
	}
}

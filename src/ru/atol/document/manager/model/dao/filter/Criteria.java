package ru.atol.document.manager.model.dao.filter;

public abstract class Criteria {

	public abstract boolean isValid();
}

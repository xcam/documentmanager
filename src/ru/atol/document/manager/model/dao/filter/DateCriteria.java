package ru.atol.document.manager.model.dao.filter;

import java.util.Date;

public class DateCriteria extends Criteria {
	private Date after;
	private Date before;

	public DateCriteria(Date after, Date before) {
		this.after = after;
		this.before = before;
	}

	@Override
	public boolean isValid() {
		return getAfter() != null && getBefore() != null;
	}

	public Date getAfter() {
		return after;
	}

	public void setAfter(Date after) {
		this.after = after;
	}

	public Date getBefore() {
		return before;
	}

	public void setBefore(Date before) {
		this.before = before;
	}
}

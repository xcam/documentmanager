package ru.atol.document.manager.model.dao.filter;

public class DocumentFilter implements Filter {

	private StringCriteria numberCriteria;
	private DateCriteria dateCriteria;

	public StringCriteria getNumberCriteria() {
		return numberCriteria;
	}

	public void setNumberCriteria(StringCriteria numberCriteria) {
		this.numberCriteria = numberCriteria;
	}

	public DateCriteria getDateCriteria() {
		return dateCriteria;
	}

	public void setDateCriteria(DateCriteria dateCriteria) {
		this.dateCriteria = dateCriteria;
	}
}

package ru.atol.document.manager.model.dao;

import ru.atol.document.manager.model.dao.localmemory.LocalMemoryDAOFactory;
import ru.atol.document.manager.model.dao.mysql.MysqlDAOFactory;
import ru.atol.document.manager.model.entity.document.ApplicationForPayment;
import ru.atol.document.manager.model.entity.document.BillOfParcels;
import ru.atol.document.manager.model.entity.document.Document;
import ru.atol.document.manager.model.entity.document.PaymentDocument;

public abstract class DAOFactory {

	private static DAOFactory localMemoryDAOFactory = new LocalMemoryDAOFactory();
	private static DAOFactory mysqlDAOFactory = new MysqlDAOFactory();

	public static DAOFactory getDefaultDAOFactory() {
		return getDAOFactory(DAOFactoryType.RDBS_MYSQL);
	}

	public static DAOFactory getDAOFactory(DAOFactoryType type) {
		switch (type) {
		case LOCAL_MEMORY:
			return localMemoryDAOFactory;

		case RDBS_MYSQL:
			return mysqlDAOFactory;
		default:
			return getDefaultDAOFactory();
		}
	}

	public void updateDocument(Document document) {
		if (document instanceof BillOfParcels) {
			getBillOfParcelsDAO().update((BillOfParcels) document);
		} else if (document instanceof ApplicationForPayment) {
			getApplicationForPaymentDAO().update(
					(ApplicationForPayment) document);
			;
		} else if (document instanceof PaymentDocument) {
			getPaymentDocumentDao().update((PaymentDocument) document);
			;
		}
	}

	public abstract DocumentDAO getDocumentDAO();

	public abstract Crud<BillOfParcels> getBillOfParcelsDAO();

	public abstract Crud<ApplicationForPayment> getApplicationForPaymentDAO();

	public abstract Crud<PaymentDocument> getPaymentDocumentDao();

}

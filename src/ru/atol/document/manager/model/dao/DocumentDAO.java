package ru.atol.document.manager.model.dao;

import ru.atol.document.manager.model.entity.document.Document;

public interface DocumentDAO extends Crud<Document>, PageableDAO<Document> {

}
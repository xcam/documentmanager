package ru.atol.document.manager.model.dao;

public enum DAOFactoryType {
	RDBS_MYSQL, LOCAL_MEMORY
}

package ru.atol.document.manager.model.dao;

import ru.atol.document.manager.model.entity.Entity;

public interface Crud<T extends Entity> {

	void saveOrUpdate(T entity);

	void delete(long id);

	void delete(T entity);

	T getByID(long id);

	void update(T entity);
}

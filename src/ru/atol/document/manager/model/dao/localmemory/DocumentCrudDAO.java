package ru.atol.document.manager.model.dao.localmemory;

import java.util.Iterator;
import java.util.List;

import ru.atol.document.manager.model.dao.Crud;
import ru.atol.document.manager.model.entity.document.Document;

public class DocumentCrudDAO<T extends Document> implements Crud<T> {
	@Override
	public void delete(long id) {
		List<Document> documents = Storage.getStorage().getDocuments();
		Iterator<Document> it = documents.iterator();
		while (it.hasNext()) {
			if (it.next().getId() == id) {
				it.remove();
			}
		}
	}

	@Override
	public void delete(T entity) {
		if (entity == null) {
			return;
		}

		delete(entity.getId());
	}

	@Override
	public void saveOrUpdate(T entity) {
		if (entity == null) {
			return;
		}

		List<Document> documents = Storage.getStorage().getDocuments();

		if (entity.getId() == 0) {
			entity.setId(Storage.nextID());
		}
		delete(entity);
		documents.add(entity);
	}

	@SuppressWarnings("unchecked")
	@Override
	public T getByID(long id) {
		List<Document> documents = Storage.getStorage().getDocuments();
		for (Document document : documents) {
			if (document.getId() == id) {
				return (T) document;
			}
		}
		return null;
	}

	@Override
	public void update(T entity) {

	}
}

package ru.atol.document.manager.model.dao.localmemory;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import ru.atol.document.manager.model.entity.document.ApplicationForPayment;
import ru.atol.document.manager.model.entity.document.BillOfParcels;
import ru.atol.document.manager.model.entity.document.Document;
import ru.atol.document.manager.model.entity.document.PaymentDocument;

public class Storage {
	private static Storage storage;
	private static long id = 1;
	private List<Document> documents;

	public Storage() {
		documents = new LinkedList<Document>();
		documents.add(generateRandomApplicationForPayment());
		documents.add(generateRandomBillOfParcels());
		documents.add(generateRandomPayment());
		documents.add(generateRandomBillOfParcels());
		documents.add(generateRandomPayment());
		documents.add(generateRandomApplicationForPayment());
		documents.add(generateRandomBillOfParcels());
		documents.add(generateRandomPayment());
		documents.add(generateRandomBillOfParcels());
		documents.add(generateRandomPayment());

		documents.add(generateRandomApplicationForPayment());
		documents.add(generateRandomBillOfParcels());
		documents.add(generateRandomPayment());
		documents.add(generateRandomBillOfParcels());
		documents.add(generateRandomPayment());
		documents.add(generateRandomApplicationForPayment());
		documents.add(generateRandomBillOfParcels());
		documents.add(generateRandomPayment());
		documents.add(generateRandomBillOfParcels());
		documents.add(generateRandomPayment());

		documents.add(generateRandomApplicationForPayment());
		documents.add(generateRandomBillOfParcels());
		documents.add(generateRandomPayment());
		documents.add(generateRandomBillOfParcels());
		documents.add(generateRandomPayment());
	}

	public static Storage getStorage() {
		if (storage == null) {
			storage = new Storage();
		}

		return storage;
	}

	public static long nextID() {
		return id++;
	}

	public List<Document> getDocuments() {
		return documents;
	}

	private BillOfParcels generateRandomBillOfParcels() {
		Random random = new Random();
		BillOfParcels bill = new BillOfParcels();
		bill.setArticle("article" + random.nextFloat());
		bill.setCurrency("currency" + random.nextFloat());
		bill.setDate(new Date());
		bill.setExchangeRate(random.nextDouble());
		bill.setId(nextID());
		bill.setNumber("BillOfParcels" + random.nextFloat());
		bill.setQuantity(random.nextDouble());
		bill.setSum(random.nextDouble());
		bill.setUser("user" + random.nextFloat());
		return bill;
	}

	private PaymentDocument generateRandomPayment() {
		Random random = new Random();
		PaymentDocument paymentDocument = new PaymentDocument();
		paymentDocument.setDate(new Date());
		paymentDocument.setEmployee("employee" + random.nextFloat());
		paymentDocument.setId(nextID());
		paymentDocument.setNumber("PaymentDocument" + random.nextFloat());
		paymentDocument.setSum(random.nextDouble());
		paymentDocument.setUser("user" + random.nextFloat());
		return paymentDocument;
	}

	private ApplicationForPayment generateRandomApplicationForPayment() {
		Random random = new Random();
		ApplicationForPayment applicationForPayment = new ApplicationForPayment();
		applicationForPayment.setContractor("contractor" + random.nextFloat());
		applicationForPayment.setCurrency("currency" + random.nextFloat());
		applicationForPayment.setDate(new Date());
		applicationForPayment.setExchangeRate(random.nextDouble());
		applicationForPayment.setFee(random.nextDouble());
		applicationForPayment.setId(nextID());
		applicationForPayment.setNumber("ApplicationForPayment"
				+ random.nextFloat());
		applicationForPayment.setSum(random.nextDouble());
		applicationForPayment.setUser("user" + random.nextFloat());
		return applicationForPayment;
	}
}

package ru.atol.document.manager.model.dao.localmemory;

import ru.atol.document.manager.model.dao.Crud;
import ru.atol.document.manager.model.dao.DAOFactory;
import ru.atol.document.manager.model.dao.DocumentDAO;
import ru.atol.document.manager.model.entity.document.ApplicationForPayment;
import ru.atol.document.manager.model.entity.document.BillOfParcels;
import ru.atol.document.manager.model.entity.document.PaymentDocument;

public class LocalMemoryDAOFactory extends DAOFactory {
	private DocumentDAO documentDAO;
	private Crud<BillOfParcels> billOfParcelsDAO;
	private Crud<ApplicationForPayment> applicationForPaymentDAO;
	private Crud<PaymentDocument> paymentDocumentDAO;

	@Override
	public DocumentDAO getDocumentDAO() {
		if (documentDAO == null) {
			documentDAO = new LocalMemoryDocumentDAO();
		}
		return documentDAO;
	}

	@Override
	public Crud<BillOfParcels> getBillOfParcelsDAO() {
		if (billOfParcelsDAO == null) {
			billOfParcelsDAO = new DocumentCrudDAO<>();
		}
		return billOfParcelsDAO;
	}

	@Override
	public Crud<ApplicationForPayment> getApplicationForPaymentDAO() {
		if (applicationForPaymentDAO == null) {
			applicationForPaymentDAO = new DocumentCrudDAO<>();
		}
		return applicationForPaymentDAO;
	}

	@Override
	public Crud<PaymentDocument> getPaymentDocumentDao() {
		if (paymentDocumentDAO == null) {
			paymentDocumentDAO = new DocumentCrudDAO<>();
		}
		return paymentDocumentDAO;
	}

}

package ru.atol.document.manager.model.dao.localmemory;

import java.util.LinkedList;
import java.util.List;

import ru.atol.document.manager.model.dao.DocumentDAO;
import ru.atol.document.manager.model.dao.PageableDAO;
import ru.atol.document.manager.model.dao.filter.Filter;
import ru.atol.document.manager.model.entity.document.Document;

public class LocalMemoryDocumentDAO extends DocumentCrudDAO<Document> implements
		DocumentDAO, PageableDAO<Document> {

	@Override
	public List<Document> getData(int offset, int size, Filter filter) {
		// TODO filter!
		List<Document> documents = Storage.getStorage().getDocuments();
		if (offset >= documents.size()) {
			return new LinkedList<Document>();
		} else {
			size += offset;
			if (size > documents.size()) {
				size = documents.size();
			}

			return documents.subList(offset, size);
		}
	}

	@Override
	public int quantity(Filter filter) {
		// TODO filter!
		List<Document> documents = Storage.getStorage().getDocuments();
		return documents.size();
	}
}
